#define __Cylinder__

#include <iostream>
#include "mathematics.h"


class Cylinder {
private:
    Vector c;
    double r;
    double h;

public:
    Cylinder();
    ~Cylinder();

};
