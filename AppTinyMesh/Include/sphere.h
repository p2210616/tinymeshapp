#ifndef __Sphere__
#define __Sphere__

#include <iostream>
#include "mathematics.h"

class Sphere {
private:
    Vector c;
    double r;
public:
    Sphere();
    ~Sphere();
};
#endif
