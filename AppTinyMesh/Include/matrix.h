#ifndef __Matrix__
#define __Matrix__

#include <iostream>

class Matrix {
private:
    int mat;
    int rows;
    int columns;
    int *matValue;
public:
    Matrix(int r, int c);
    ~Matrix();
    int& operator()(int r, int c);
    int& operator()(int r, int c)const;
    Matrix& operator +=(int scale);
    Matrix operator+(int scale);
    Matrix& operator -=(int scale);
    Matrix operator -(int scale);
    Matrix& operator *=(int scale);
    Matrix operator *(int scale);
    Matrix& operator *=(const Matrix& m);
    Matrix operator *(const Matrix& m);
    Matrix& operator +=(const Matrix& m);
    Matrix operator +(const Matrix& m);
    Matrix Transpose(const Matrix& m);
    Matrix Inverse(const Matrix& m);

};
#endif
