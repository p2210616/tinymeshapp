#include "matrix.h"


Matrix::Matrix(int r, int c) {
    rows = r;
    columns = c;
    mat = rows*columns;
    matValue = new int[mat]();
}

Matrix::~Matrix() {
    if(matValue) {
        delete [] matValue;
    }
}

int& Matrix:: operator()(int r, int c) {
    if(rows >= r || columns>=c) {
        std::out_of_range("Index out of bounds");
    }
    return matValue[rows*c+columns];
}

int& Matrix:: operator()(int r, int c) const{
    if(rows >= r || columns>=c) {
        std::out_of_range("Index out of bounds");
    }
    return matValue[rows*c+columns];
}

Matrix& Matrix:: operator +=(int scale) {
    for(int i=0; i<mat; i++) {
        matValue[i] += scale;
    }
    return *this;
}

Matrix Matrix::operator +(int scale) {
    Matrix res(rows, columns);

    for(int i=0; i<mat; i++) {
        res.matValue[i] = matValue[i] + scale;
    }
    return res;
}

Matrix& Matrix::operator -=(int scale) {
    for(int i=0; i<mat; i++) {
        matValue[i] -= scale;
    }
    return *this;
}

Matrix Matrix::operator -(int scale) {
    Matrix res(rows, columns);

    for(int i=0; i<mat; i++) {
        res.matValue[i] = matValue[i] - scale;
    }

    return res;
}

Matrix& Matrix::operator *=(int scale) {
    for(int i=0; i<mat; i++) {
        matValue[i] *= scale;
    }
    return *this;
}

Matrix Matrix::operator *(int scale) {
    Matrix res(rows, columns);

    for(int i=0; i<mat; i++) {
        res.matValue[i] = matValue[i] * scale;
    }
    return res;
}

Matrix& Matrix::operator *=(const Matrix& m) {
    Matrix res = (*this) * m;
    (*this) = res;
    return (*this);
}

Matrix Matrix::operator *(const Matrix& m) {
    if(columns != m.rows) {
        std::length_error("Matrice shapes error");
    }
    Matrix res(rows, m.columns);
    int sum;
    for(int i=0; i<rows; i++) {
        for(int j=0; j<m.columns; j++) {
            sum = 0;
            for (int k=0; k<rows; k++) {
                sum += this->operator()(i,k) * m(k, j);
            }
            res(i, j) = sum;
        }
    }
    return res;
}

Matrix& Matrix::operator +=(const Matrix& m) {
    Matrix res = (*this) + m;
    (*this) = res;
    return (*this);
}

Matrix Matrix::operator +(const Matrix& m) {
    if(rows != m.rows || columns != m.columns) {
        std::length_error("Matrice shapes error");
    }
    Matrix res(rows, columns);
    for(int i=0; i<mat; i++) {
        res.matValue[i] = this->matValue[i] + m.matValue[i];
    }
    return res;
}

Matrix Matrix::Transpose(const Matrix& m) {
    Matrix res(rows, columns);

    for(int i=0; i<m.rows; i++) {
        for(int j=0; j<m.columns; j++) {
            //res[j][i] = m[i][j] ; //TODO define [] operator
        }
    }
    return res;

}

Matrix Matrix::Inverse(const Matrix& m) {
    Matrix res(rows, columns);
    //TODO return value
    return res;
}

